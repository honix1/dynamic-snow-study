﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowDown : MonoBehaviour {

	void FixedUpdate() {
		GetComponent<Rigidbody>().angularVelocity *= 0.9f;
	}
}
